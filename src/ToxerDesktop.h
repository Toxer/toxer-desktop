/*
 * This file is part of the Toxer application, a Tox messenger client.
 *
 * Copyright (c) 2019-2023 Nils Fenner <nils@macgitver.org>
 *
 * This software is licensed under the terms of the MIT license:
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#pragma once

// FIXME: Always include headers from "toxercore" prefix.
#ifdef SUBDIR_TOXERCORE
#  include "Settings.h"
#else
#  include "toxercore/Settings.h"
#endif
#include "ToxerTheme.h"

#include <QStringBuilder>
#include <QtQml/qqml.h>
#include <QUrl>


class QClipboard;

namespace Toxer {

inline QUrl pathTo(const QString& filename) {
  return QUrl(u"qrc:/ToxerDesktop/"_qs % filename);
}

} // namespace Toxer

namespace ToxerDesktop {

void registerQmlTypes();

class Clipboard : public QObject {
  Q_OBJECT
public:
  explicit Clipboard(QObject* parent = nullptr);

  Q_INVOKABLE QString text() const;
  Q_INVOKABLE void setText(const QString& text);

private:
  QClipboard* acquireClipboard();

private:
  QClipboard* c_;
};

class UiSettings : public QObject, Toxer::Settings {
  Q_OBJECT
  Q_PROPERTY(quint8 app_layout READ app_layout_int WRITE set_app_layout NOTIFY app_layout_changed FINAL)
  Q_PROPERTY(bool fullscreen READ fullscreen WRITE set_fullscreen NOTIFY fullscreen_changed FINAL)
  Q_PROPERTY(QRect geometry READ geometry WRITE set_geometry NOTIFY geometry_changed FINAL)
public:
  enum class AppLayout : quint8 { Slim, Split };
  Q_ENUM(AppLayout)

public:
  UiSettings(QObject* parent = nullptr);

  Q_INVOKABLE AppLayout app_layout() const;
  Q_INVOKABLE void set_app_layout(AppLayout layout);
  Q_INVOKABLE quint8 app_layout_int() const;
  Q_INVOKABLE void set_app_layout(quint8 layout);

  Q_INVOKABLE bool fullscreen() const;
  Q_INVOKABLE void set_fullscreen(bool enabled);

  Q_INVOKABLE QRect geometry() const;
  Q_INVOKABLE void set_geometry(const QRect& rect);

signals:
  void app_layout_changed(quint8 layout_enum);
  void fullscreen_changed(bool fullscreen);
  void geometry_changed(const QRect& rect);
};

inline void registerQmlTypes() {
  qmlRegisterType<Clipboard>("Toxer.Desktop",1,0,"Clipboard");
  qmlRegisterType<UiSettings>("Toxer.Settings",1,0,"UiSettings");

  qmlRegisterUncreatableType<Colors>("Toxer.Colors",1,0,"ToxerColors",u"Toxer.Colors is a subtype for Toxer.Theme."_qs);
  qmlRegisterUncreatableType<Theme>("Toxer.Theme",1,0,"ToxerTheme",u"Toxer.Theme is not a QML creatable type."_qs);
}

} // namespace ToxerDesktop
