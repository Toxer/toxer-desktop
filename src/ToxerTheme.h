#pragma once

#include <QColor>
#include <QMap>
#include <QObject>
#include <QProperty>
#include <QtQml/qqml.h>
#include <QUrl>

// FIXME: Always include headers from "toxercore" prefix.
#ifdef SUBDIR_TOXERCORE
#  include "ToxTypes.h"
#else
#  include "toxercore/ToxTypes.h"
#endif

namespace ToxerDesktop {

class Theme;

class Colors : public QObject {
  Q_OBJECT

  QProperty<QColor> accent_;                                       Q_PROPERTY(QColor accent READ accent NOTIFY accentChanged)
  QProperty<QColor> base_ { QColor::fromHslF(0.0f, 0.0f, 0.1f) };  Q_PROPERTY(QColor base READ base WRITE setBase NOTIFY baseChanged BINDABLE bindableBase)
  QProperty<float>  base_hue_;                                     Q_PROPERTY(float baseHue READ baseHue WRITE setBaseHue NOTIFY baseHueChanged BINDABLE bindableBaseHue)
  QProperty<float>  base_saturation_;                              Q_PROPERTY(float baseSaturation READ baseSaturation WRITE setBaseSaturation NOTIFY baseSaturationChanged BINDABLE bindableBaseSaturation)
  QProperty<float>  base_lightness_;                               Q_PROPERTY(float baseLightness  READ baseLightness WRITE setBaseLightness NOTIFY baseLightnessChanged BINDABLE bindableBaseLightness)
  QProperty<QColor> contrast_;                                     Q_PROPERTY(QColor contrast READ contrast NOTIFY contrastChanged BINDABLE bindableContrast)
  QProperty<QColor> hover_;                                        Q_PROPERTY(QColor hover READ hover NOTIFY hoverChanged BINDABLE bindableHover)
  QProperty<bool>   light_theme_ { false };                        Q_PROPERTY(bool lightTheme READ lightTheme WRITE setLightTheme NOTIFY lightThemeChanged BINDABLE bindableLightTheme)
  QProperty<QColor> nomatch_ { QColor(0x80, 0x30, 0x30) };         Q_PROPERTY(QColor nomatch  READ nomatch  WRITE setNomatch NOTIFY nomatchChanged BINDABLE bindableNomatch)
  QProperty<QColor> text_;                                         Q_PROPERTY(QColor text READ text NOTIFY textChanged BINDABLE bindableText)
  QProperty<QColor> valid_ { QColor(0x30, 0x80, 0x30) };           Q_PROPERTY(QColor valid READ valid WRITE setValid NOTIFY validChanged BINDABLE bindableValid)

signals:
  void accentChanged();
  void baseChanged();
  void baseHueChanged();
  void baseLightnessChanged();
  void baseSaturationChanged();
  void contrastChanged();
  void hoverChanged();
  void lightThemeChanged();
  void nomatchChanged();
  void textChanged();
  void validChanged();

public:
  Colors(QObject* parent=nullptr);

  inline QBindable<QColor> bindableAccent() { return &accent_; }
  inline QColor accent() { return accent_; }

  inline QBindable<bool> bindableBase() { return &base_; }
  inline QColor base() { return base_; }
  inline void setBase(const QColor &val) { base_ = val; }

  inline QBindable<QColor> bindableContrast() { return &contrast_; }
  inline QColor contrast() const { return contrast_; }

  inline QBindable<QColor> bindableHover() { return &hover_; }
  inline QColor hover() const { return hover_; }

  inline QBindable<bool> bindableLightTheme() { return &light_theme_; }
  inline bool lightTheme() { return light_theme_; }
  inline void setLightTheme(bool val) { light_theme_ = val; }

  inline QBindable<bool> bindableNomatch() { return &nomatch_; }
  inline auto nomatch() { return nomatch_.value(); }
  inline void setNomatch(QColor val) { nomatch_ = val; }

  inline QBindable<QColor> bindableText() { return &text_; }
  inline auto text() const { return text_.value(); }

  inline QBindable<bool> bindableValid() { return &valid_; }
  inline QColor valid() { return valid_; }
  inline void setValid(QColor val) { valid_ = val; }

  inline QBindable<float> bindableBaseHue() { return &base_hue_; }
  inline float baseHue() const { return base_hue_; }
  inline void setBaseHue(float val) {
    auto c = base_.value();
    val = qBound(0.0f, val, 1.0f);
    if (val != base_hue_.value()) {
      auto l = c.lightnessF();
      auto s = c.hslSaturationF();
      c.setHslF(val, s, l);
    }
    base_ = c;
    base_hue_ = val;
  }

  inline QBindable<float> bindableBaseSaturation() { return &base_saturation_; }
  inline float baseSaturation() const { return base_saturation_; }
  inline void setBaseSaturation(float val) {
    auto c = base_.value();
    val = qBound(0.0f, val, 1.0f);
    if (val != base_saturation_.value()) {
      auto h = c.hslHueF();
      auto l = c.lightnessF();
      c.setHslF(h, val, l);
    }
    base_ = c;
    base_saturation_ = val;
  }

  inline QBindable<float> bindableBaseLightness() { return &base_lightness_; }
  inline float baseLightness() const { return base_lightness_; }
  inline void setBaseLightness(float val) {
    auto c = base_.value();
    val = qBound(0.0f, val, 1.0f);
    if (val != c.lightnessF()) {
      auto h = c.hslHueF();
      auto s = c.hslSaturationF();
      c.setHslF(h, s, val);
    }
    base_ = c;
    base_lightness_ = val;
  }

  Q_INVOKABLE QColor tox_status(const QString &key) const {
    if (key == u"disabled"_qs) { return QColor::fromString(u"gray"_qs); }
    if (key == u"enabled"_qs)  { return QColor::fromString(u"#80ff80"_qs); }
    if (key == u"pending"_qs)  { return QColor::fromString(u"#bbbb80"_qs); }
    if (key == u"active"_qs)   { return QColor::fromString(u"#ff8080"_qs); }

    qCritical("Invalid tox_status color key -> %s", qUtf8Printable(key));
    return QColor();
  }

  Q_INVOKABLE QColor friend_status(const QString &key) {
    if (key == u"offline"_qs) { return QColor::fromString(u"gray"_qs); }
    if (key == u"online"_qs)  { return QColor::fromString(u"#80ff80"_qs); }
    if (key == u"away"_qs)    { return QColor::fromString(u"#bbbb80"_qs); }
    if (key == u"busy"_qs)    { return QColor::fromString(u"#ff8080"_qs); }

    qCritical("Invalid friend_status color key -> %s", qUtf8Printable(key));
    return QColor();
  }
};


class Theme : public QObject {
  Q_OBJECT

  Q_PROPERTY(Colors* color READ colors NOTIFY colorsChanged)

  QProperty<qreal> font_size_ {8.0};  Q_PROPERTY(qreal fontPointSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged BINDABLE fontSizeBindable)

  ToxerDesktop::Colors* colors_;
  const QMap<QString, QString> icon_map_ = {
    {u"info"_qs       , u"contact.svg"_qs},
    {u"noAvatar"_qs   , u"contact.svg"_qs},
    {u"offline"_qs    , u"dot_offline.svg"_qs},
    {u"online"_qs     , u"dot_online.svg"_qs},
    {u"away"_qs       , u"dot_away.svg"_qs},
    {u"busy"_qs       , u"dot_busy.svg"_qs},
    {u"sendMessage"_qs, u"send_message.svg"_qs},
    {u"settings"_qs   , u"settings.svg"_qs},
    {u"add"_qs        , u"add.svg"_qs},
  };

signals:
  void colorsChanged();
  void fontSizeChanged();

public:
  Theme(QObject* parent=nullptr);

  inline auto colors() const { return colors_; }

  inline qreal fontSize() const { return font_size_; }
  inline QBindable<qreal> fontSizeBindable() { return &font_size_; }
  inline void setFontSize(qreal val) { font_size_ = val; }

  inline QString icons_path() const { return u"qrc:/images/"_qs + (colors_->property("lightTheme").toBool() ? u"light"_qs : u"dark"_qs); }
  Q_INVOKABLE QUrl getIcon(const QString& key) const {
    if (!icon_map_.contains(key)) {
      return {};
    }

    return QString::fromUtf8("%1/%2").arg(icons_path()).arg(icon_map_[key]);
  }

  Q_INVOKABLE QUrl getAvailability(bool isOnline, ToxTypes::UserStatus status) const {
    if (isOnline) {
      switch (status) {
      case ToxTypes::UserStatus::Unknown:
      case ToxTypes::UserStatus::Away: return getIcon(u"away"_qs);
      case ToxTypes::UserStatus::Busy: return getIcon(u"busy"_qs);
      case ToxTypes::UserStatus::Ready: return getIcon(u"online"_qs);
      }
      qWarning("No icon for unknown availability %s", qUtf8Printable(ToxTypes::enumKey(status)));
      return getIcon(u"away"_qs);
    }
    return getIcon(u"offline"_qs);
  }
};

} // namespace ToxerDesktop
