import QtQuick
import QtQuick.Controls.Material
import QtQuick.Controls
import QtQuick.Effects

import "animations" as Animations
import "controls" as Controls

Controls.Page {
  id: root
  width: Math.min(500, Screen.width)
  height: Math.min(600, Screen.height)

  footer: Rectangle { id: footer_container
    implicitHeight: footer_layout.implicitHeight
    color: '#567'

    Column {
      id: footer_layout
      spacing: 0
      Label {
        id: txt_toxer_version
        text: Qt.application.name + " " + Qt.application.version
      }
      Label {
        id: txt_toxcore_version
        text: "Based on Tox " + Toxer.toxVersionString()
      }
    }
  }
  header: Item { id: header_container
    implicitHeight: root.height / 2.5

    Image { id: logo
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.bottom: parent.bottom
      height: parent.height - 70
      width: height
      sourceSize: Qt.size(width, height)
      source: "qrc:/images/light/login_logo.svg"
      cache: false
    }
    MultiEffect {
      source: logo
      anchors.fill: logo
      brightness: 0.2
      saturation: 0.2
      blurEnabled: true
      blurMax: 32
      blur: 1.0
    }
  }

  function startToxSession() {
    Toxer.activateProfile(txt_profile.currentText, txt_password.text)
  }

  Animations.Ringing {
    target: logo
    duration: 100
    loops: 12
    running: true
  }

  Column {
    id: profile_selector
    width: 200
    anchors.centerIn: parent
    spacing: 8

    ComboBox {
      id: txt_profile
      width: profile_selector.width
      model: Toxer.availableProfiles()
    }
    TextField {
      id: txt_password
      width: profile_selector.width
      echoMode: TextInput.Password
      placeholderText: qsTr("Enter the profile's password")
    }
    Button {
      id: btnStart
      Accessible.defaultButton: true
      width: profile_selector.width
      anchors.horizontalCenter: profile_selector.horizontalCenter
      text: qsTr("Unlock Profile")
      enabled: txt_password.length > 0
      onClicked: { root.startToxSession(); }
    }
  }
}
