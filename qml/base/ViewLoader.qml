import QtQuick

Loader {
  id: root
  width: 100
  height: 100
  focus: true
  onItemChanged: {
    if (item) {
      // note: item must be a Base.View
      item.viewLoader = root
    } else if (defaultView){
      root.source = root.defaultView
    }
  }
  Component.onCompleted: { root.source = root.defaultView; }

  property url defaultView: ""
}
