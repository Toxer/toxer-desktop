import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts

import Toxer.Desktop

import "controls" as Controls
import "friend" as Friend

Controls.Page {
  id: root
  width: Math.min(600, Screen.width)
  height: Math.min(600, Screen.height)
  background: Rectangle { color: 'transparent' }

  Clipboard { id: clipboard }

  ButtonGroup {
    id: mainToolButtons
    exclusive: true
    onCheckedButtonChanged: {
      if (checkedButton == btn_settings) {
        stack_view.push("settings/Overview.qml")
        //viewLoader.source = "settings/Overview.qml"
      } else if (checkedButton == btn_invite_friend) {
        stack_view.push("friend/Invite.qml")
        //viewLoader.source = "friend/Invite.qml"
      } else if (checkedButton == btnFriendInfo) {
        stack_view.push("friend/Info.qml")
        //viewLoader.setSource("friend/Info.qml", { friendNo: friends.selectedFriend })
      } else {
        stack_view.clear()
        //viewLoader.sourceComponent = null
      }
    }
  }

  SplitView {
    id: h_split
    anchors.fill: parent
    orientation: Qt.Horizontal
    focus: true

    Item {
      id: layoutWrapper
      implicitWidth: root.width * 0.3
      SplitView.minimumWidth: root.width * 0.1
      SplitView.maximumWidth: root.width * 0.8
      SplitView.fillHeight: true

      ColumnLayout {
        anchors.fill: parent
        spacing: 2

        Rectangle {
          id: quickActions
          Layout.fillWidth: true
          Layout.preferredHeight: Math.max(parent.height * 0.05, 32)
          color: Material.primary
          clip: true

          Row {
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            spacing: 0

            RoundButton {
              id: btn_invite_friend
              icon.source: Style.getIcon('add')
              checkable: true
              ButtonGroup.group: mainToolButtons
              background: Rectangle {
                radius: btn_invite_friend.radius
                color: Style.color.contrast
              }

               ScaleAnimator {
                 target: btn_invite_friend.contentItem
                 duration: 200
                 alwaysRunToEnd: true
                 running: btn_invite_friend.checked
               }
            }

            RoundButton {
              id: btn_friend_info
              icon.source: Style.getIcon('info')
              checkable: true
              ButtonGroup.group: mainToolButtons
              enabled: friends.selectedFriend !== -1
              background: Rectangle {
                radius: btn_friend_info.radius
                color: Style.color.contrast
              }
            }

            RoundButton {
              id: btn_settings
              icon.source: Style.getIcon('settings')
              checkable: true
              ButtonGroup.group: mainToolButtons
              background: Rectangle {
                radius: btn_settings.radius
                color: Style.color.contrast
              }

              RotationAnimator {
                target: btn_settings.contentItem
                duration: 2000
                from: 0
                to: 360
                loops: Animation.Infinite
                running: btn_settings.checked
              }
            }
          }
        }

        CurrentProfile {
          id: my_profile
          Layout.fillWidth: true
          Layout.minimumHeight: Math.max(root.height * 0.1)
          Layout.maximumHeight: 30

          Menu {
            id: self_menu

            MenuItem {
              text: qsTr("Copy Tox-Address to Clipboard")
              onTriggered: clipboard.setText(my_profile.tox.addressStr().toUpperCase())
            }
          }

          MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton
            onClicked: (mouse)=> {
              if (mouse.button === Qt.RightButton) {
                self_menu.popup()
              }
            }
          }
        }

        Friend.List {
          id: friends
          Layout.fillWidth: true
          Layout.fillHeight: true
          //viewLoader: viewLoader
        }
      }
    }

    StackView {
      id: stack_view
      clip: true
      SplitView.fillWidth: true
    }
    Text {
      text: `Stacked: ${stack_view.depth}`
      color: Material.primary
    }
//    Base.ViewLoader {
//      id: viewLoader

//      Connections {
//        target: viewLoader.item
//        function onClosing() { mainToolButtons.checkedButton = null; }
//      }
//    }
  }
}
