import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Effects
import QtQuick.Layouts

import "../animations"

Item {
  id: root
  implicitWidth: 200
  implicitHeight: infoColumn.height

  property alias img_avatar: img_avatar
  property alias img_status_light: img_status_light
  property alias txt_name: txt_name
  property alias txt_status_message: txt_status_message

  property int unreadMessages: 0

  Pulsing {
    target: unread_messages_effect
    properties: "shadowBlur"
    from: 0.3
    to: 0.75
    loops: Animation.Infinite
    running: unread_messages_effect.visible
  }
  RowLayout {
    anchors.fill: parent
    spacing: 0

    Item {
      id: avatar_item
      Layout.fillHeight: true
      implicitWidth: height + img_status_light.width

      Image {
        id: img_avatar
        visible: false
        height: parent.height
        width: height
        fillMode: Image.PreserveAspectFit
        sourceSize: Qt.size(width, height)
      }
      MultiEffect {
        source: img_avatar
        anchors.fill: img_avatar
        maskEnabled: true
        maskSource: Image {
          source: "qrc:/masks/circle.svg"
        }
      }
      Image {
        id: img_status_light
        visible: true
        height: Math.max(img_avatar.height * 0.33, 10)
        width: height
        anchors.bottom: img_avatar.bottom
        anchors.right: img_avatar.right
        anchors.rightMargin: -(height / 2)
        fillMode: Image.PreserveAspectFit
        sourceSize: Qt.size(width, height)
      }
      MultiEffect {
        id: unread_messages_effect
        source: img_status_light
        anchors.fill: img_status_light
        blurMax: 10
        shadowColor: "#6a3"
        shadowEnabled: true
        visible: unreadMessages > 0
      }
    }
    Column {
      id: infoColumn
      Layout.minimumWidth: 20
      Layout.fillWidth: true

      Label {
        id: txt_name
        width: parent.width
        font.bold: true
        text: "<alias>"
      }

      Label {
        id: txt_status_message
        width: parent.width
        text: "<status_message>"
      }
    }
  }
}
