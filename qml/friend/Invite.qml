import QtQuick
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts

import Toxer

import "../controls" as Controls

Controls.Page {
  id: root

  ToxProfileQuery { id: tpq }
  ColumnLayout {
    anchors.fill: parent

    Label {
      text: qsTr("Invite a friend to join your Tox network.")
    }
    Row {
      id: row_addr
      Layout.fillWidth: true

      Rectangle {
        width: tav_indicator.width + 6
        height: txt_tox_addr.height
        color: txt_tox_addr.acceptableInput ? Style.color.valid : Style.color.nomatch

        Label {
          id: tav_indicator
          anchors.centerIn: parent
          text: txt_tox_addr.acceptableInput ? "VALID" : "INVALID"
        }
      }
      TextField {
        id: txt_tox_addr
        width: row_addr.width
        placeholderText: qsTr("Enter a Tox address.")
        validator: ToxAddressValidator { id: tav }
      }
    }
    Button {
      id: btn_invite
      Layout.alignment: Layout.Center
      Accessible.defaultButton: true
      enabled: txt_tox_addr.text && txt_tox_addr.acceptableInput
      text: qsTr("Send Tox Invitation")
      onClicked: { tpq.inviteFriend(txt_tox_addr.text, "Please add me as Tox friend.\n\nRegards,\n" + tpq.userName()); }
    }
    Button {
      id: btn_close
      Layout.alignment: Layout.Center
      text: qsTr("Close")
      //onClicked: { closing(); }
    }
  }
}
