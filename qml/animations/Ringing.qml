import QtQuick

SequentialAnimation {
  id: root

  property var target: null
  property int duration: 100
  property real radius: 4
  property real scale: 1.04

  readonly property int internalDuration: root.duration / 6

  ScaleAnimator {
    target: root.target
    from: target.scale
    to: root.scale
    duration: root.internalDuration
  }
  RotationAnimator {
    target: root.target
    duration: root.internalDuration
    to: root.radius
  }
  RotationAnimator {
    target: root.target
    duration: root.internalDuration
    to: 0
  }
  RotationAnimator {
    target: root.target
    duration: root.internalDuration
    to: -root.radius
  }
  RotationAnimator {
    target: root.target
    duration: root.internalDuration
    to: 0
  }
  ScaleAnimator {
    target: root.target
    from: root.scale
    to: target.scale
    duration: root.internalDuration
  }
}
