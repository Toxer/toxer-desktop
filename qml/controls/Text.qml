import QtQuick as Q

Q.Text {
  maximumLineCount: 1
  wrapMode: NoWrap
  textFormat: PlainText
  elide: ElideRight
  color: Style.color.text
  font.pointSize: Style.fontPointSize
}
